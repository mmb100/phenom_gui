import attr
import GPy
import numpy as np


@attr.s
class Approx():
   #Takes in a phenotype object.. What does ib do? 
    phenotype = attr.ib()
    x_kernel = attr.ib(default=GPy.kern.RBF)

    def kernels(self):
        kx = self.x_kernel(1)

        kdesign = None
        # What is L?
        for i in range(self.phenotype.design.L):

            # select indices that belong to this prior?
            ind, = np.where(self.phenotype.design.priors == i)

            if kdesign is None:
                kdesign = GPy.kern.Linear(
                    ind.shape[0], active_dims=ind, ARD=False)
            else:
                ktmp = GPy.kern.Linear(
                    ind.shape[0], active_dims=ind, ARD=False)
                kdesign = kdesign + ktmp

        return kx, kdesign

    def model(self, optimize=True
              """ 
            Kronecker Gaussian regression since it is more computationally efficient. 
            This is to create the gaussian process model and optimize it. 
              """
        kx, kdesign = self.kernels()
        #Config holds parameters for the model such as priors
        cfg = self.phenotype.config()

        m = GPy.models.GPKroneckerGaussianRegression(
            cfg['x'][:, None], cfg['design'], cfg['y'].T, kx, kdesign
        )

        if optimize:
            
            m.optimize()

        return m

    def posterior(self, m, p):
        """
        
        """
        design = self.phenotype.design
        priors = design.priors

        ind, = np.where(priors == p)
        # 
        u = np.unique(design.matrix[:, priors == p], axis=0)

        mus = []
        vars = []
         # Use the optimize model to predict on whatever you have in x.

        for i in range(u.shape[0]):
            # Create design Matrix
            predx2 = np.zeros((1, design.matrix.shape[1]))
            predx2[0, ind] = u[i, :]
            # Make prediction on design matrix
            mu, var = m.predict(m.X1, predx2)

            mus.append(mu[:, 0])
            vars.append(var[:, 0])
        # Returns samples from posterior distribution 
        return np.column_stack(mus), np.column_stack(var)
